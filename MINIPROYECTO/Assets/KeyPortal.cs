using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPortal : MonoBehaviour
{
    public GameObject Objetollave;
    public GameObject Portal;
    // Start is called before the first frame update
    void Start()
    {
        Portal.SetActive(false);   
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            Destroy(Objetollave);
            Portal.SetActive(true);
        }
    }

}
