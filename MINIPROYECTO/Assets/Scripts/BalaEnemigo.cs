using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigo : MonoBehaviour
{
    // Start is called before the first frame update
    public float Velocidad;
    public int da�o;

    private void Update()
    {
        transform.Translate(Time.deltaTime * Velocidad * -Vector2.up);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.PerderVida();
            Destroy(gameObject);
        }
    }
}
