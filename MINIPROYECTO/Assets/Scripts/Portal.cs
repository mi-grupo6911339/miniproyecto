using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    public string sceneName;
    //public GameObject portalEffect;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            LoadNextLevel();
        }
    }


    private void LoadNextLevel()
    {
        // Carga la siguiente escena
        SceneManager.LoadScene(sceneName);
    }
}